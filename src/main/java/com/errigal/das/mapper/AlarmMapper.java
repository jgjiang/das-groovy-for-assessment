package com.errigal.das.mapper;


import com.errigal.das.model.Alarm;
import org.apache.ibatis.annotations.*;

import java.util.List;


public interface AlarmMapper {

    @Select("select count(*) from alarm where node_id = #{node_id}")
    Integer countAlarmsByNodeId(@Param("node_id") Integer nodeId);

    @Select("select count(*) from alarm where hub_id = #{hub_id} and node_id is null")
    Integer countHubAlarmsByHubId(@Param("hub_id") Integer hubId);

    @Select("select count(*) from alarm where hub_id = #{hub_id}")
    Integer countHubAndNodeAlarmsByHubId(@Param("hub_id") Integer hubId);


    @Delete("DELETE FROM alarm WHERE node_id = #{node_id}")
    Integer deleteAlarmsByNodeId(@Param("node_id") Integer nodeId);

    @Delete("delete from alarm where hub_id = #{hub_id} and node_id is null")
    Integer deleteHubAlarmsByHubId(@Param("hub_id") Integer hubId);

    @Delete("delete from alarm where hub_id = #{hub_id}")
    Integer deleteHubAndNodeAlarmsByHubId(@Param("hub_id") Integer hubId);

    @Insert("insert into alarm(alarm_type, num, remedy, hub_id, node_id) values (#{alarmType}, #{num}, #{remedy}, #{hubId}, null)")
    Integer addHubAlarm(Alarm alarm);

    @Insert("insert into alarm(alarm_type, num, remedy, hub_id, node_id) values (#{alarmType}, #{num}, #{remedy}, #{hubId}, #{nodeId})")
    Integer addNodeAlarm(Alarm alarm);

    @Select("select * from alarm where node_id = #{node_id}")
    @Results({
            @Result(property = "alarmType", column = "alarm_type"),
            @Result(property = "num", column = "num"),
            @Result(property = "remedy", column = "remedy"),
            @Result(property = "hubId", column = "hub_id"),
            @Result(property = "nodeId", column = "node_id")
    })
    List<Alarm> findNodeAlarmsByNodeId(@Param("node_id") Integer nodeId);

    @Select("select * from alarm where hub_id = #{hub_id} and node_id is null")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "alarmType", column = "alarm_type"),
            @Result(property = "num", column = "num"),
            @Result(property = "remedy", column = "remedy"),
            @Result(property = "hubId", column = "hub_id"),
            @Result(property = "nodeId", column = "node_id")
    })
    List<Alarm> findHubAlarmsByHubId(@Param("hub_id") Integer hubId);

}