package com.errigal.das.mapper;

import com.errigal.das.model.Hub;
import com.errigal.das.model.Node;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.ArrayList;
import java.util.List;

public interface NodeMapper {

    @Select("select * from node where hub_id = #{hub_id}")
    @Results({
            @Result(property = "nodeName", column = "node_name"),
            @Result(property = "nodeIdentifier", column = "node_identifier"),
            @Result(property = "hubId", column = "hub_id")
    })
    List<Node> findNodesByHubId(@Param("hub_id") Integer id);

    @Select("select * from node where node_name = #{node_name}")
    @Results(value = {
            @Result(property = "nodeName", column = "node_name"),
            @Result(property = "nodeIdentifier", column = "node_identifier"),
            @Result(property = "hubId", column = "hub_id"),

        })
    Node findNodeByNodeName(@Param("node_name") String nodeName);


    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "nodeName", column = "node_name"),
            @Result(property = "nodeIdentifier", column = "node_identifier"),
            @Result(property = "hubId", column = "hub_id"),
            @Result(property = "alarms", column = "id", many = @Many(select = "com.errigal.das.mapper.AlarmMapper.findNodeAlarmsByNodeId", fetchType = FetchType.LAZY))
    })
    @Select("select * from node where id = #{node_id}")
    Node findNodeByNodeId(@Param("node_id") Integer nodeId);

    @Insert("insert into node(node_name, node_identifier, hub_id) values (#{nodeName}, #{nodeIdentifier}, #{hubId})")
    Integer addNode(Node node);

    @Delete("DELETE FROM node WHERE node_name = #{node_name}")
    Integer deleteNodeByNodeName(@Param("node_name") String nodeName);

    @Delete("DELETE FROM node WHERE hub_id = #{hub_id}")
    Integer deleteNodesByHubId(@Param("hub_id") Integer hubId);

    @Select("select count(*) from node where hub_id = #{hub_id}")
    Integer countNodesByHubId(@Param("hub_id") Integer hubId);

    @Update("update node set node_name = #{nodeName} where id = #{id}")
    Integer updateNodeNameByNodeId(Node node);



}
