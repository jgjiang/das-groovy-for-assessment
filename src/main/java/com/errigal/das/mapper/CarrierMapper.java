package com.errigal.das.mapper;

import com.errigal.das.model.Carrier;
import com.errigal.das.model.Hub;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;


public interface CarrierMapper {

    @Select("select * from carrier")
    @Results(
            @Result(property = "carrierName", column = "carrier_name")
    )
    List<Carrier> findAllCarriers();

    @Select("SELECT * FROM carrier WHERE id = #{id}")
    @Results(
            @Result(property = "carrierName", column = "carrier_name")
    )
    Carrier findCarrierNameById(@Param("id") Integer id);

    @Select("SELECT * FROM carrier WHERE carrier_name = #{carrier_name}")
    @Results(
            @Result(property = "carrierName", column = "carrier_name")
    )
    Carrier findCarrierByName(@Param("carrier_name") String carrier_name);

    @Select("SELECT id FROM carrier WHERE carrier_name = #{carrier_name}")
    Integer findCarrierIdByName(@Param("carrier_name") String carrier_name);

    @Insert("insert into carrier(carrier_name) values (#{carrierName})")
    Integer addCarrier(Carrier carrier);

    @Delete("DELETE FROM carrier WHERE carrier_name = #{carrier_name}")
    Integer deleteCarrier(@Param("carrier_name") String carrier_name);

    @Update("update carrier set carrier_name = #{carrierName} where id = #{id}")
    Integer updateCarrierNameByCarrierId(Carrier carrier);

    @Select("select * from carrier where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "carrierName", column = "carrier_name"),
            @Result(property = "hubs", column = "id", many = @Many(select =
                    "com.errigal.das.mapper.HubMapper.findHubsByCarrierId", fetchType = FetchType.LAZY))

    })
    Carrier findCarrierAndChildrenByCarrierId(@Param("id") Integer CarrierId);


}
