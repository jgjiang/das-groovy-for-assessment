package com.errigal.das.mapper;

import com.errigal.das.model.Hub;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public interface HubMapper {

    @Select("select * from hub where carrier_id = #{carrier_id}")
    @Results({
            @Result(property = "hubName", column = "hub_name"),
            @Result(property = "hubIdentifier", column = "hub_identifier"),
            @Result(property = "carrierId", column = "carrier_id"),
    })
    List<Hub> findHubsByCarrierId( @Param("carrier_id") Integer id);

    @Select("select * from hub where hub_name = #{hub_name}")
    @Results({
            @Result(property = "hubName", column = "hub_name"),
            @Result(property = "hubIdentifier", column = "hub_identifier"),
            @Result(property = "carrierId", column = "carrier_id")
    })
    Hub findHubByHubName(@Param("hub_name") String hubName);

    @Select("select * from hub where id = #{id}")
    @Results({
            @Result(property = "hubName", column = "hub_name"),
            @Result(property = "hubIdentifier", column = "hub_identifier"),
            @Result(property = "carrierId", column = "carrier_id")
    })
    Hub findHubByHubId(@Param("id") Integer id);

    @Insert("insert into hub(hub_name, hub_identifier, carrier_id) values (#{hubName}, #{hubIdentifier}, #{carrierId})")
    Integer addHub(Hub hub);

    @Delete("delete from hub where hub_name = #{hub_name}")
    Integer deleteHubByHubName(@Param("hub_name") String hubName);

    @Update("update hub set hub_name = #{hubName} where id = #{id}")
    Integer updateHubNameByHubId(Hub hub);

    @Select("select * from hub where carrier_id in (select id from carrier where carrier_name = #{carrier_name})")
    @Results({
            @Result(property = "hubName", column = "hub_name"),
            @Result(property = "hubIdentifier", column = "hub_identifier"),
            @Result(property = "carrierId", column = "carrier_id")
    })
    List<Hub> findHubsByCarrierName(@Param("carrier_name") String carrierName);


    @Select("select * from hub where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "hubName", column = "hub_name"),
            @Result(property = "hubIdentifier", column = "hub_identifier"),
            @Result(property = "carrierId", column = "carrier_id"),
            @Result(property = "alarms", column = "id", many = @Many(select =
                    "com.errigal.das.mapper.AlarmMapper.findHubAlarmsByHubId", fetchType = FetchType.LAZY)),
            @Result(property = "nodes", column = "id", many = @Many(select =
                    "com.errigal.das.mapper.NodeMapper.findNodesByHubId", fetchType = FetchType.LAZY))
    })
    Hub findHubAndChildrenByHubId(@Param("id") Integer hubId);


}
