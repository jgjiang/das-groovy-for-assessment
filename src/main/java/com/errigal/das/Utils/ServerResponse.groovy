package com.errigal.das.Utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL) // if null object, don't serialize the object
class ServerResponse  implements Serializable {
    def status, msg, data

    // constructors
    private ServerResponse(status) {
        this.status = status
    }

    private ServerResponse(status, data) {
        this.status = status
        this.data = data
    }

    private ServerResponse(status, msg, data) {
        this.status = status
        this.msg = msg
        this.data = data
    }


    // helper method to check successful response
    @JsonIgnore
    boolean isSuccess() {
        this.status == Const.SUCCESS;
    }

    // successful response

    def static createBySuccessMsg(msg) {
        new ServerResponse(Const.SUCCESS, msg)
    }

    def static createBySuccess(data) {
        new ServerResponse<>(Const.SUCCESS, data)
    }

    def static createBySuccess(String msg, data) {
        new ServerResponse<>(Const.SUCCESS, msg, data)
    }

    // error response
    def static createByErrorMsg(String errorMsg) {
        return new ServerResponse(Const.ERROR, errorMsg)
    }


}
