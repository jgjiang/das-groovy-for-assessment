package com.errigal.das.Utils;

public class Const {
     public static final String UNIT_UNAVAILABLE = "unit unavailable";
     public static final String  OPTICAL_LOSS = "optical loss";
     public static final String DARK_FIBRE = "dark fibre";
     public static final String POWER_OUTAGE = "power outage";

     public static final int SUCCESS = 0;
     public static final int ERROR = 1;


}
