package com.errigal.das.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(value = "handler")
class Hub {

    def id, hubName, hubIdentifier, carrierId, alarms, nodes

    Hub() {
        super()
    }


}
