package com.errigal.das.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(value = "handler")
class Alarm {
    def id, alarmType, num, remedy, hubId, nodeId

    Alarm() {
        super()
    }


}
