package com.errigal.das.Service;

import com.errigal.das.Utils.ServerResponse;
import com.errigal.das.model.Node;
import org.apache.ibatis.annotations.Insert;

import java.util.List;

interface INodeService {
    def findNodesByHubId(Integer hubId)
    def addNode(String nodeName, String nodeIdentifier, String hubName)
    def deleteNode(String nodeName)
    def updateNodeName(Integer id, String newNodeName)
    def findNodeByNodeId(Integer nodeId)
}
