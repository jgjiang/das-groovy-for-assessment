package com.errigal.das.Service;

import com.errigal.das.Utils.ServerResponse;
import com.errigal.das.model.Carrier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

interface ICarrierService {

    def findAllCarriers()
    def findCarrierById(Integer id)
    def findCarrierByName(String Name)
    def showNetwork()

    def addCarrier(String carrierName)
    def deleteCarrier(String carrierName)
    def updateCarrierName(Integer carrierId, String newCarrierName)
    def findCarrierAndChildrenByCarrierId(Integer carrierId)

}
