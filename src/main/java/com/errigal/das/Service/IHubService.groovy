package com.errigal.das.Service;

import com.errigal.das.Utils.ServerResponse;
import com.errigal.das.model.Hub;

import java.util.List;

interface IHubService {
    def findHubsByCarrierId(Integer carrierId)
    def findHubsByCarrierName(String name)
    def addHub(String hubName, String hubIdentifier, String carrierName)
    def deleteHub(String hubName)
    def updateHubName(Integer hubId, String newHubName)
    def findHubAndChildrenByHubId(Integer hubId)
}
