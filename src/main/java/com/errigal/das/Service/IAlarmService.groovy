package com.errigal.das.Service;

import com.errigal.das.Utils.ServerResponse;

interface IAlarmService {
    def addHubAlarm(String alarmType, Integer num, String remedy, Integer hubId)
    def addNodeAlarm(String alarmType, Integer num, String remedy, Integer hubId, Integer nodeId)
    def deleteAlarmsOnNode(Integer nodeId)
    def deleteAlarmsOnHub(Integer hubId)

}
