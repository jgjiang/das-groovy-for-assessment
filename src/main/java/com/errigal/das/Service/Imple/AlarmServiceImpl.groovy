package com.errigal.das.Service.Imple;

import com.errigal.das.Service.IAlarmService;
import com.errigal.das.Utils.Const;
import com.errigal.das.Utils.ServerResponse;
import com.errigal.das.mapper.AlarmMapper;
import com.errigal.das.model.Alarm;
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AlarmServiceImpl implements IAlarmService{

    @Autowired
    private AlarmMapper alarmMapper

    @Override
    def addHubAlarm(String alarmType, Integer num, String remedy, Integer hubId) {
        def alarm = new Alarm(num: num, remedy: remedy, hubId: hubId)
        setAlarmType(alarm, alarmType)

        def rowCount = alarmMapper.addHubAlarm(alarm)
        println "$rowCount"
        if (rowCount) {
           return ServerResponse.createBySuccess("add hub alarm successfully")
        }
        return ServerResponse.createByErrorMsg("fail to add hub alarm. sorry")
    }

    @Override
    def addNodeAlarm(String alarmType, Integer num, String remedy, Integer hubId, Integer nodeId) {
        def alarm = new Alarm(num: num, remedy: remedy, hubId: hubId, nodeId: nodeId)
        setAlarmType(alarm, alarmType)
        def rowCount = alarmMapper.addNodeAlarm(alarm)
        if (rowCount) {
           return ServerResponse.createBySuccess("add node alarm successfully")
        }

        return ServerResponse.createByErrorMsg("fail to add node alarm. sorry")
    }

    @Override
    def deleteAlarmsOnNode(Integer nodeId) {
        def nodeAlarmCount = alarmMapper.countAlarmsByNodeId(nodeId)
        if (nodeAlarmCount) {
            Integer rowCount = alarmMapper.deleteAlarmsByNodeId(nodeId)
            if (rowCount) {
               return ServerResponse.createBySuccess("delete alarms on this node successfully")
            }
        } else {
           return ServerResponse.createByErrorMsg("no alarms attached to this node")
        }
    }

    @Override
    def deleteAlarmsOnHub(Integer hubId) {
        def hubAlarmCount = alarmMapper.countHubAlarmsByHubId(hubId)
        if (hubAlarmCount) {
            Integer rowCount = alarmMapper.deleteHubAlarmsByHubId(hubId)
            if (rowCount) {
               return ServerResponse.createBySuccess("delete alarms on this hub successfully")
            }
        } else {
           return ServerResponse.createByErrorMsg("no alarms attached to this hub")
        }

    }


    // helper method to setAlarmType
    static ServerResponse setAlarmType(Alarm alarm, String alarmType) {
        switch (alarmType) {
            case Const.DARK_FIBRE:
                alarm.alarmType = Const.DARK_FIBRE
                break
            case Const.OPTICAL_LOSS:
                alarm.alarmType = Const.OPTICAL_LOSS
                break
            case Const.POWER_OUTAGE:
                alarm.alarmType = Const.POWER_OUTAGE
                break
            case Const.UNIT_UNAVAILABLE:
                alarm.alarmType = Const.UNIT_UNAVAILABLE
                break
            default:
                return ServerResponse.createByErrorMsg("please choose right alarm type")
        }

        return null
    }



}
