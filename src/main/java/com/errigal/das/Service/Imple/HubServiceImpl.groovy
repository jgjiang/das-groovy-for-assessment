package com.errigal.das.Service.Imple;

import com.errigal.das.Service.IHubService;
import com.errigal.das.Service.INodeService;
import com.errigal.das.Utils.ServerResponse;
import com.errigal.das.mapper.AlarmMapper;
import com.errigal.das.mapper.CarrierMapper;
import com.errigal.das.mapper.HubMapper;
import com.errigal.das.mapper.NodeMapper;
import com.errigal.das.model.Hub;
import com.errigal.das.model.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
class HubServiceImpl implements IHubService {

    @Autowired
    private HubMapper hubMapper

    @Autowired
    private CarrierMapper carrierMapper

    @Autowired
    private NodeMapper nodeMapper

    @Autowired
    private AlarmMapper alarmMapper

    @Autowired
    private INodeService iNodeService

    @Override
   def findHubsByCarrierId(Integer carrierId) {
        def list = hubMapper.findHubsByCarrierId(carrierId)
        if (list.empty) {
            return ServerResponse.createByErrorMsg("no hubs in this carrier!")
        }
        return ServerResponse.createBySuccess(list)
    }

    @Override
    def findHubsByCarrierName(String name) {
        def id = carrierMapper.findCarrierIdByName(name);
        def list = hubMapper.findHubsByCarrierId(id);
        if (CollectionUtils.isEmpty(list)) {
            return ServerResponse.createByErrorMsg("no hubs in this carrier!")
        }

        return ServerResponse.createBySuccess(list)
    }

    @Override
    def addHub(String hubName, String hubIdentifier, String carrierName) {

        def carrierId = carrierMapper.findCarrierIdByName(carrierName)
        if (!carrierId) {
            return ServerResponse.createByErrorMsg("no such carrier in system");
        }

        Hub hub = new Hub(hubName: hubName, hubIdentifier: hubIdentifier, carrierId: carrierId)
        def rowCount = hubMapper.addHub(hub)
        if (rowCount) {
            return ServerResponse.createBySuccess("add hub successfully")
        }
        return ServerResponse.createByErrorMsg("fail to add hub, sorry!")
    }

    @Override
    def deleteHub(String hubName) {

        Hub hub = hubMapper.findHubByHubName(hubName)
        if (!hub) {
            return ServerResponse.createByErrorMsg("no such hub in system")
        }

        // delete alarms attached to this hub and alarms attached to the nodes of this hub
        def hubId = hubMapper.findHubByHubName(hubName).id
        def hubAndNodeAlarmCount = alarmMapper.countHubAndNodeAlarmsByHubId(hubId)
        if (hubAndNodeAlarmCount) {
            if (alarmMapper.deleteHubAndNodeAlarmsByHubId(hubId)) {
                System.out.println("alarms are deleted")
            }
        }

        // delete nodes of this hub
        def nodeCount = nodeMapper.countNodesByHubId(hubId)
        if (nodeCount) {
            if (nodeMapper.deleteNodesByHubId(hubId)) {
                System.out.println("hub's nodes are deleted")
            }
        }

        // delete this hub
        def rowCount = hubMapper.deleteHubByHubName(hubName)
        if (rowCount) {
            return ServerResponse.createBySuccess("delete hub successfully")
        }
        return ServerResponse.createByErrorMsg("fail to delete this hub")
    }

    @Override
    def updateHubName(Integer hubId, String newHubName) {
        def hub = hubMapper.findHubByHubId(hubId)
        if (!hub) {
            return ServerResponse.createByErrorMsg("no such hub in system")
        }
        hub.hubName = newHubName
        def rowCount = hubMapper.updateHubNameByHubId(hub)
        if (rowCount) {
            return ServerResponse.createBySuccess("update hubName successfully")
        }

        return ServerResponse.createByErrorMsg("fail to update hubName.")
    }

    @Override
    def findHubAndChildrenByHubId(Integer hubId) {
        def hub = hubMapper.findHubAndChildrenByHubId(hubId)
        if (!hub) {
            return ServerResponse.createByErrorMsg("no such hub in system")
        }
        def nodeList = hub.nodes
        if (!nodeList.empty) {
            nodeList.each {
                node -> node.alarms = iNodeService.findNodeByNodeId(node.id).data.alarms
            }
        }
        return ServerResponse.createBySuccess(hub)
    }

}
