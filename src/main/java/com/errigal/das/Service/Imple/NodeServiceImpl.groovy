package com.errigal.das.Service.Imple;

import com.errigal.das.Service.INodeService;
import com.errigal.das.Utils.ServerResponse;
import com.errigal.das.mapper.AlarmMapper;
import com.errigal.das.mapper.HubMapper;
import com.errigal.das.mapper.NodeMapper;
import com.errigal.das.model.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;


@Service
class NodeServiceImpl implements INodeService {

    @Autowired
    private NodeMapper nodeMapper

    @Autowired
    private HubMapper hubMapper

    @Autowired
    private AlarmMapper alarmMapper

    @Override
    def findNodesByHubId(Integer hubId) {
        def list = nodeMapper.findNodesByHubId(hubId)
        if (list.empty) {
            return ServerResponse.createByErrorMsg("no nodes in this hub!")
        }

        return ServerResponse.createBySuccess(list);
    }

    @Override
    def addNode(String nodeName, String nodeIdentifier, String hubName) {
        if (!hubMapper.findHubByHubName(hubName)) {
            return ServerResponse.createByErrorMsg("no such hub in system!")
        }
        def hubId = hubMapper.findHubByHubName(hubName).id
        def node = new Node(nodeName: nodeName, nodeIdentifier: nodeIdentifier, hubId: hubId)

        def rowCount = nodeMapper.addNode(node)
        if (rowCount) {
            return ServerResponse.createBySuccess("add node successfully")
        }

        return ServerResponse.createByErrorMsg("fail to add node, sorry")
    }

    @Override
    def deleteNode(String nodeName) {
        def node = nodeMapper.findNodeByNodeName(nodeName)
        if (!node) {
            return ServerResponse.createByErrorMsg("no such node in system")
        }

        // check if any alarms attached with this node
        def rowCount = alarmMapper.countAlarmsByNodeId(node.id)
        if (rowCount) {
            alarmMapper.deleteAlarmsByNodeId(node.id)
        }

        def deletedNodeRowCount = nodeMapper.deleteNodeByNodeName(nodeName)
        if (deletedNodeRowCount) {
            return ServerResponse.createBySuccess("delete node and its alarms successfully")
        }

        return ServerResponse.createByErrorMsg("fail to delete node")
    }

    @Override
    def updateNodeName(Integer id, String newNodeName) {
        def node = nodeMapper.findNodeByNodeId(id)
        if (!node) {
            return ServerResponse.createByErrorMsg("no such node in system")
        }
        node.nodeName = newNodeName
        def rowCount = nodeMapper.updateNodeNameByNodeId(node)
        if (rowCount) {
            return ServerResponse.createBySuccess("update nodeName successfully")
        }

        return ServerResponse.createByErrorMsg("fail to update node name.")
    }

    @Override
    def findNodeByNodeId(Integer nodeId) {
        def node = nodeMapper.findNodeByNodeId(nodeId)
        if (!node) {
            return ServerResponse.createByErrorMsg("no such node in system")
        }
        return ServerResponse.createBySuccess(node)
    }
}
