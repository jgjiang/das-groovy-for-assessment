package com.errigal.das.Service.Imple;

import com.errigal.das.Service.ICarrierService;
import com.errigal.das.Service.IHubService;
import com.errigal.das.Service.INodeService;
import com.errigal.das.Utils.ServerResponse;
import com.errigal.das.mapper.CarrierMapper;
import com.errigal.das.mapper.HubMapper;
import com.errigal.das.mapper.NodeMapper;
import com.errigal.das.model.Carrier;
import com.errigal.das.model.Hub;
import com.errigal.das.model.Node;
import com.fasterxml.jackson.databind.util.JSONPObject;
import elemental.json.JsonObject;
import org.apache.catalina.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
class CarrierServiceImpl implements ICarrierService{

    @Autowired
    private CarrierMapper carrierMapper

    @Autowired
    private HubMapper hubMapper

    @Autowired
    private NodeMapper nodeMapper

    @Autowired
    private INodeService iNodeService

    @Autowired
    private IHubService iHubService


    @Override
    def findAllCarriers() {
       def list = carrierMapper.findAllCarriers()
       if (list.empty) {
           return ServerResponse.createByErrorMsg("no carriers in the Network!")
       }
       return ServerResponse.createBySuccess(list)
    }

    @Override
    def findCarrierById(Integer id) {
        def carrier = carrierMapper.findCarrierNameById(id)
        if (!carrier) {
            return ServerResponse.createByErrorMsg("no carrier found!")
        }

        return ServerResponse.createBySuccess(carrier)
    }

    @Override
    def findCarrierByName(String name) {
        def carrier = carrierMapper.findCarrierByName(name)
        if (!carrier) {
            return ServerResponse.createByErrorMsg("no carrier found!")
        }

        return ServerResponse.createBySuccess(carrier)
    }


    @Override
    def showNetwork() {
        def carrierList = carrierMapper.findAllCarriers()
        if (!carrierList.empty) {
            carrierList.each {
                carrier -> carrier.hubs = this.findCarrierAndChildrenByCarrierId(carrier.id).data.hubs
            }
        }
        return ServerResponse.createBySuccess(carrierList)
    }

    @Override
    def addCarrier(String carrierName) {
        def carrier = new Carrier(carrierName: carrierName)
        def rowCount = carrierMapper.addCarrier(carrier)
        if (rowCount) {
            return ServerResponse.createBySuccess("add carrier successfully")
        }
        return ServerResponse.createByErrorMsg("fail to add carrier! sorry!")
    }


    @Override
    def deleteCarrier(String carrierName) {
        if (!carrierMapper.findCarrierByName(carrierName)) {
            return ServerResponse.createByErrorMsg("no such carrier in system.")
        }

        // check if any hubs attached to this carrier
        def hubList = hubMapper.findHubsByCarrierName(carrierName)

        // if no hubs, delete carrier directly
        if (hubList.empty) {
            Integer deletedRowCountOfCarrier = carrierMapper.deleteCarrier(carrierName)
            if (deletedRowCountOfCarrier) {
                return ServerResponse.createBySuccess("delete carrier successfully")
            }
        }

        // loop hubList to remove hubs, nodes and alarms
        hubList.each {
            hub ->
                def nodeList = nodeMapper.findNodesByHubId(hub.id)
                if (CollectionUtils.isEmpty(nodeList)) {
                    iHubService.deleteHub(hub.hubName)
                }
                nodeList.each {
                    node -> iNodeService.deleteNode(node.nodeName)
                }
                iHubService.deleteHub(hub.hubName)
        }


        // delete carrier
        int rowCount = carrierMapper.deleteCarrier(carrierName);
        if (rowCount) {
            return ServerResponse.createBySuccess("delete carrier successfully")
        }

        return ServerResponse.createByErrorMsg("fail to delete carrier! sorry!")
    }

    @Override
    def updateCarrierName(Integer carrierId, String newCarrierName) {
        def carrier = carrierMapper.findCarrierNameById(carrierId)
        if (!carrier) {
           return ServerResponse.createByErrorMsg("no carrier found!")
        }
        carrier.carrierName = newCarrierName
        Integer rowCount = carrierMapper.updateCarrierNameByCarrierId(carrier)
        if (rowCount) {
           return ServerResponse.createBySuccess("update carrierName successfully")
        }

        return ServerResponse.createByErrorMsg("fail to update carrierName, sorry!")
    }

    @Override
    def findCarrierAndChildrenByCarrierId(Integer carrierId) {
        def carrier = carrierMapper.findCarrierAndChildrenByCarrierId(carrierId)
        if (!carrier) {
           return ServerResponse.createByErrorMsg("no such carrier in system")
        }

        def hubsList = carrier.hubs
        hubsList.each {
            hub ->
                hub.alarms = iHubService.findHubAndChildrenByHubId(hub.id).data.alarms
                hub.nodes = iHubService.findHubAndChildrenByHubId(hub.id).data.nodes
        }
        return ServerResponse.createBySuccess(carrier)
    }

}
