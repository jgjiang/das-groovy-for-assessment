package com.errigal.das.Controllers

import com.errigal.das.Service.ICarrierService
import com.errigal.das.Service.IHubService
import com.errigal.das.Service.INodeService
import com.errigal.das.Utils.ServerResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

@Controller
@RequestMapping("/nodes")
class NodeController {

    @Autowired
    private INodeService iNodeService

    @RequestMapping("find_nodes_by_hubId.do")
    @ResponseBody
    def getNodesByHubId(Integer id) {
        return iNodeService.findNodesByHubId(id)
    }

    @RequestMapping("add_node.do")
    @ResponseBody
    def addNode(String nodeName, String nodeIdentifier, String hubName) {
        if (StringUtils.isEmpty(nodeName) || StringUtils.isEmpty(hubName)) {
             return ServerResponse.createByErrorMsg("please enter valid nodeName or hubName")
        }
        return iNodeService.addNode(nodeName, nodeIdentifier,hubName)
    }

    @RequestMapping("delete_node.do")
    @ResponseBody
    def deleteNode(String nodeName) {
        if (StringUtils.isEmpty(nodeName)) {
            return ServerResponse.createByErrorMsg("please enter valid node name")
        }
        return iNodeService.deleteNode(nodeName)
    }

    @RequestMapping("update_node_name.do")
    @ResponseBody
    def updateNodeName(Integer id, String newNodeName) {
        if (!id || StringUtils.isEmpty(newNodeName)) {
             return ServerResponse.createByErrorMsg("id or newNodeName is invalid")
        }
        return iNodeService.updateNodeName(id,newNodeName)
    }

    @RequestMapping("show_node_and_alarms.do")
    @ResponseBody
    def showNodeAndAlarms(Integer nodeId) {
        if (!nodeId) {
            return ServerResponse.createByErrorMsg("nodeId is invalid")
        }
        return iNodeService.findNodeByNodeId(nodeId)
    }

}
