package com.errigal.das.Controllers;


import com.errigal.das.Service.IAlarmService
import com.errigal.das.Utils.ServerResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

@Controller
@RequestMapping("/alarms")
class AlarmController {

    @Autowired
    private IAlarmService iAlarmService

    @RequestMapping("add_hub_alarm.do")
    @ResponseBody
    def addHubAlarm(String alarmType, Integer num, String remedy, Integer hubId) {
        if (StringUtils.isEmpty(alarmType) || !hubId) {
            return ServerResponse.createByErrorMsg("please enter valid alarmType or hubId")
        }
       return iAlarmService.addHubAlarm(alarmType, num, remedy, hubId)
    }

    @RequestMapping("add_node_alarm.do")
    @ResponseBody
    def addNodeAlarm(String alarmType, Integer num, String remedy, Integer hubId, Integer nodeId) {
        if (StringUtils.isEmpty(alarmType) || !hubId || !nodeId) {
            return ServerResponse.createByErrorMsg("please enter valid alarmType, hubId or nodeId");
        }
        return iAlarmService.addNodeAlarm(alarmType, num, remedy, hubId, nodeId)
    }

    @RequestMapping("delete_alarms_on_node.do")
    @ResponseBody
    def deleteAlarmsOnNode(Integer nodeId) {
        if (!nodeId) {
            return ServerResponse.createByErrorMsg("please enter valid nodeId")
        }
        return iAlarmService.deleteAlarmsOnNode(nodeId)
    }

    @RequestMapping("delete_alarms_on_hub.do")
    @ResponseBody
    def deleteAlarmsOnHub(Integer hubId) {
        if (!hubId) {
            return ServerResponse.createByErrorMsg("please enter valid hubId")
        }
        return iAlarmService.deleteAlarmsOnHub(hubId)
    }
}
