package com.errigal.das.Controllers


import com.errigal.das.Service.IHubService

import com.errigal.das.Utils.ServerResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody


@Controller
@RequestMapping("/hubs")
class HubController {

    @Autowired
    private IHubService iHubService

    @RequestMapping("find_hubs_by_id.do")
    @ResponseBody
    def getHubsById(Integer id) {
        if (!id) {
            return ServerResponse.createByErrorMsg("carrierId is invalid")
        }
        return iHubService.findHubsByCarrierId(id)
    }

    @RequestMapping("find_hubs_by_name.do")
    @ResponseBody
    def getHubsByCarrierName(String name) {
        return iHubService.findHubsByCarrierName(name)
    }

    @RequestMapping("add_hub.do")
    @ResponseBody
    def addHub(String hubName, String hubIdentifier, String carrierName) {
        if (StringUtils.isEmpty(carrierName) || StringUtils.isEmpty(hubName)) {
            return ServerResponse.createByErrorMsg("please enter valid hubName or carrierName")
        }

        return iHubService.addHub(hubName,hubIdentifier,carrierName)
    }

    // test to do...
    @RequestMapping("delete_hub.do")
    @ResponseBody
    def deleteHub(String hubName) {
        if (StringUtils.isEmpty(hubName)) {
            return ServerResponse.createByErrorMsg("please enter valid hub name")
        }
        return iHubService.deleteHub(hubName)
    }

    @RequestMapping("update_hub_name.do")
    @ResponseBody
    def updateHubName(Integer hubId, String newHubName) {
        if (!hubId || StringUtils.isEmpty(newHubName)) {
            return ServerResponse.createByErrorMsg("hubId or new name is invalid")
        }
        return iHubService.updateHubName(hubId,newHubName)
    }

    @RequestMapping("show_hub_and_children.do")
    @ResponseBody
    def showHubAndChildren(Integer hubId) {
        if (!hubId) {
            return ServerResponse.createByErrorMsg("hubId is invalid")
        }
        return iHubService.findHubAndChildrenByHubId(hubId)
    }
}
