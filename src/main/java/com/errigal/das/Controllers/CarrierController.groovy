package com.errigal.das.Controllers;

import com.errigal.das.Service.ICarrierService;
import com.errigal.das.Service.IHubService;
import com.errigal.das.Service.INodeService;
import com.errigal.das.Utils.ServerResponse;
import com.errigal.das.mapper.CarrierMapper;
import com.errigal.das.model.Carrier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping("/carriers")
class CarrierController {

    @Autowired
    private ICarrierService iCarrierService;

    @RequestMapping("show_network.do")
    @ResponseBody
    def showNetwork() {
        iCarrierService.showNetwork()
    }

    @RequestMapping("find_all.do")
    @ResponseBody
    def getAllCarriers() {
        iCarrierService.findAllCarriers()
    }

    @RequestMapping("find_carrier_by_id.do")
    @ResponseBody
    def getCarrierById(Integer id) {
        iCarrierService.findCarrierById(id)
    }

    @RequestMapping("find_carrier_by_name.do")
    @ResponseBody
    def getCarrierByName(String name) {
        if (StringUtils.isEmpty(name)) {
            return ServerResponse.createByErrorMsg("please enter valid name.")
        }
        return iCarrierService.findCarrierByName(name);
    }

    @RequestMapping(value = "add_carrier.do", method = RequestMethod.POST)
    @ResponseBody
    def addCarrier(String carrierName) {
        if (StringUtils.isEmpty(carrierName)) {
            return ServerResponse.createByErrorMsg("please enter valid carrier name")
        }
        return iCarrierService.addCarrier(carrierName)
    }

    @RequestMapping("delete_carrier.do")
    @ResponseBody
    def deleteCarrier(String carrierName) {
        if (StringUtils.isEmpty(carrierName)) {
            return ServerResponse.createByErrorMsg("please enter valid carrier name")
        }
        return iCarrierService.deleteCarrier(carrierName)
    }

    @RequestMapping("update_carrier_name.do")
    @ResponseBody
    def updateCarrierName(Integer id, String newCarrierName) {
        if (!id || StringUtils.isEmpty(newCarrierName)) {
            return ServerResponse.createByErrorMsg("carrierId or new name is invalid.")
        }
        return iCarrierService.updateCarrierName(id, newCarrierName)
    }

    @RequestMapping("show_carrier_and_children.do")
    @ResponseBody
    def showCarrierAndChildren(Integer carrierId) {
        if (!carrierId) {
           return ServerResponse.createByErrorMsg("carrierId is invalid")
        }
        return iCarrierService.findCarrierAndChildrenByCarrierId(carrierId)
    }



}
