package com.errigal.das;

import com.errigal.das.mapper.CarrierMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.errigal.das.mapper")
public class DasApplication {

	public static void main(String[] args) {
		SpringApplication.run(DasApplication.class, args);
	}

}
