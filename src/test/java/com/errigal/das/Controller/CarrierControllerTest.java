package com.errigal.das.Controller;

import com.errigal.das.Controllers.CarrierController;
import com.errigal.das.Service.ICarrierService;
import com.errigal.das.Utils.ServerResponse;
import com.errigal.das.model.Carrier;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;



@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class CarrierControllerTest {
    @Autowired
    private CarrierController carrierController;

    @Autowired
    private ICarrierService iCarrierService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mvc;
    private String expectedJson;

    @Before
    public void setUp() throws JsonProcessingException {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    // GET Tests

    @Test
    public void testShowNetwork() throws Exception {
        ServerResponse serverResponse = (ServerResponse)iCarrierService.showNetwork();
        expectedJson = Obj2Json(serverResponse);

        String uri = "/carriers/show_network.do";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();

        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        Assert.assertTrue("correct", expectedJson.equals(content));
        Assert.assertFalse("error", !expectedJson.equals(content));
        System.out.println(content);
    }

    @Test
    public void testGetAllCarriers() throws Exception {

        ServerResponse serverResponse = (ServerResponse)iCarrierService.findAllCarriers();
        expectedJson = Obj2Json(serverResponse);

        String uri = "/carriers/find_all.do";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();

        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        Assert.assertTrue("correct", expectedJson.equals(content));
        Assert.assertFalse("error", !expectedJson.equals(content));
        System.out.println(content);
    }


    @Test
    public void testGetCarrierById() throws Exception {
        ServerResponse serverResponse = (ServerResponse)iCarrierService.findCarrierById(1);
        expectedJson = Obj2Json(serverResponse);
        String uri = "/carriers/find_carrier_by_id.do?id=1";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();

        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        Assert.assertTrue("correct", expectedJson.equals(content));
        Assert.assertFalse("error", !expectedJson.equals(content));
        System.out.println(content);
    }

    @Test
    public void testGetCarrierByName() throws Exception {
        ServerResponse serverResponse = (ServerResponse) iCarrierService.findCarrierByName("Three");
        expectedJson = Obj2Json(serverResponse);
        String uri = "/carriers/find_carrier_by_name.do?name=Three";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();

        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        Assert.assertTrue("correct", expectedJson.equals(content));
        Assert.assertFalse("error", !expectedJson.equals(content));
        System.out.println(content);
    }


    @Test
    public void testShowCarrierAndChildren() throws Exception {
        ServerResponse serverResponse = (ServerResponse)iCarrierService.findCarrierAndChildrenByCarrierId(1);
        expectedJson = Obj2Json(serverResponse);
        String uri = "/carriers/show_carrier_and_children.do?carrierId=1";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();

        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        Assert.assertTrue("correct", expectedJson.equals(content));
        Assert.assertFalse("error", !expectedJson.equals(content));
        System.out.println(content);
    }


    // POST Tests
    @Test
    public void testAddCarrier() throws Exception {

        // test params
        String testCarrierName = "T-Mobile";

        String uri = "/carriers/add_carrier.do";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri).param("carrierName", testCarrierName))
                .andReturn();

        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        System.out.println(content);

    }

    @Test
    public void testUpdateCarrierName() throws Exception {
        // test params
        String testNewCarrierName = "opennetTest";
        Integer carrierId = 1;

        String uri = "/carriers/update_carrier_name.do";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri)
                .param("id", String.valueOf(carrierId))
                .param("newCarrierName", testNewCarrierName))
                .andReturn();

        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        System.out.println(content);
    }


    // DELETE Tests
    @Test
    public void testDeleteCarrier() throws Exception {
        // test params
        String carrierName = "T-Mobile";

        String uri = "/carriers/delete_carrier.do";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.delete(uri)
                .param("carrierName",carrierName)).andReturn();
        Integer status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        System.out.println(content);
    }


    private String Obj2Json(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }


}
