package com.errigal.das.Controller;

import com.errigal.das.Controllers.CarrierController;
import com.errigal.das.Controllers.HubController;
import com.errigal.das.Service.ICarrierService;
import com.errigal.das.Service.IHubService;
import com.errigal.das.Utils.ServerResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class HubControllerTest {

    @Autowired
    private HubController hubController;

    @Autowired
    private IHubService iHubService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mvc;
    private String expectedJson;

    @Before
    public void setUp() throws JsonProcessingException {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    // GET Tests
    @Test
    public void testGetHubsById() throws Exception{
        Integer carrierId = 1;
        ServerResponse serverResponse = (ServerResponse) iHubService.findHubsByCarrierId(carrierId);
        expectedJson = Obj2Json(serverResponse);
        String uri = "/hubs/find_hubs_by_id.do?id=" + String.valueOf(carrierId);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();

        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        Assert.assertTrue("correct", expectedJson.equals(content));
        Assert.assertFalse("error", !expectedJson.equals(content));
        System.out.println(content);
    }

    @Test
    public void testGetHubsByCarrierName() throws Exception {
        String carrierName = "Three";
        ServerResponse serverResponse = (ServerResponse) iHubService.findHubsByCarrierName(carrierName);
        expectedJson = Obj2Json(serverResponse);
        String uri = "/hubs/find_hubs_by_name.do?name=" + carrierName;
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();

        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        Assert.assertTrue("correct", expectedJson.equals(content));
        Assert.assertFalse("error", !expectedJson.equals(content));
        System.out.println(content);

    }

    @Test
    public void testShowHubAndChildren() throws Exception {
        // test params
        Integer hubId = 1;

        ServerResponse serverResponse = (ServerResponse) iHubService.findHubAndChildrenByHubId(hubId);
        expectedJson = Obj2Json(serverResponse);
        String uri = "/hubs/show_hub_and_children.do?hubId=" + String.valueOf(hubId);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();

        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        Assert.assertTrue("correct", expectedJson.equals(content));
        Assert.assertFalse("error", !expectedJson.equals(content));
        System.out.println(content);
    }


    // POST Tests
    @Test
    public void testAddHub() throws Exception {
        //test params
        String hubName = "testHub1";
        String hubIdentifier = "testHubIdentifier";
        String carrierName = "Three";

        String uri = "/hubs/add_hub.do";
        int status = this.mvc.perform(MockMvcRequestBuilders.post(uri)
                .param("hubName", hubName)
                .param("hubIdentifier", hubIdentifier)
                .param("carrierName", carrierName))
                .andReturn().getResponse().getStatus();
        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);

    }


    // PUT Tests
    @Test
    public void testUpdateHubName() throws Exception {
        // test params
        Integer hubId = 146;
        String newHubName = "newHubNameForTest";

        String uri = "/hubs/update_hub_name.do";
        int status = this.mvc.perform(MockMvcRequestBuilders.post(uri)
                .param("hubId", String.valueOf(hubId))
                .param("newHubName", newHubName))
                .andReturn().getResponse().getStatus();
        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
    }


    // DELETE Tests
    @Test
    public void testDeleteHub() throws Exception {
        // test params
        String hubName = "testHub1";

        String uri = "/hubs/delete_hub.do";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.delete(uri)
                .param("hubName",hubName)).andReturn();
        Integer status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        System.out.println(content);
    }

    private String Obj2Json(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }


}
