package com.errigal.das.Controller;

import com.errigal.das.Service.IAlarmService;
import com.errigal.das.Utils.Const;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class AlarmControllerTest {

    @Autowired
    private IAlarmService iAlarmService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mvc;
    private String expectedJson;

    @Before
    public void setUp() throws JsonProcessingException {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    // POST Test
    @Test
    public void testAddHubAlarm() throws Exception{
        //test params
        String alarmType = Const.DARK_FIBRE;
        Integer num = 21;
        String remedy = "testRemedy";
        Integer hubId = 1;

        String uri = "/alarms/add_hub_alarm.do";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri)
                .param("alarmType", alarmType)
                .param("num", String.valueOf(num))
                .param("remedy", remedy)
                .param("hubId", String.valueOf(hubId)))
                .andReturn();
        Integer status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        System.out.println(content);
    }

    @Test
    public void testAddNodeAlarm() throws Exception {

        // test params
        String alarmType = Const.DARK_FIBRE;
        Integer num = 1000;
        String remedy = "testRemedyForNode";
        Integer hubId = 1;
        Integer nodeId = 3;

        String uri = "/alarms/add_node_alarm.do";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri)
                .param("alarmType", alarmType)
                .param("num", String.valueOf(num))
                .param("remedy", remedy)
                .param("hubId", String.valueOf(hubId))
                .param("nodeId", String.valueOf(nodeId)))
                .andReturn();
        Integer status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        System.out.println(content);
    }

    @Test
    public void testDeleteAlarmsOnNode() throws Exception {
        //test params
        Integer nodeId = 5;

        String uri = "/alarms/delete_alarms_on_node.do";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.delete(uri)
                .param("nodeId",String.valueOf(nodeId))).andReturn();
        Integer status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        System.out.println(content);
    }

    @Test
    public void testDeleteAlarmsOnHub() throws Exception {
        //test params
        Integer hubId = 147;

        String uri = "/alarms/delete_alarms_on_hub.do";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.delete(uri)
                .param("hubId",String.valueOf(hubId))).andReturn();
        Integer status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        System.out.println(content);
    }

}
