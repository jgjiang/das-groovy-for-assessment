package com.errigal.das.Controller;

import com.errigal.das.Service.INodeService;
import com.errigal.das.Utils.ServerResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class NodeControllerTest {

    @Autowired
    private INodeService iNodeService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mvc;
    private String expectedJson;

    @Before
    public void setUp() throws JsonProcessingException {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    //GET Tests

    @Test
    public void testGetNodesByHubId() throws Exception {
        //test params
        Integer id = 1;

        ServerResponse serverResponse = (ServerResponse) iNodeService.findNodesByHubId(id);
        expectedJson = Obj2Json(serverResponse);
        String uri = "/nodes/find_nodes_by_hubId.do?id=" + id;
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();

        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        Assert.assertTrue("correct", expectedJson.equals(content));
        Assert.assertFalse("error", !expectedJson.equals(content));
        System.out.println(content);
    }


    @Test
    public void testShowNodeAndAlarms() throws Exception {
        //test params
        Integer nodeId = 3;

        ServerResponse serverResponse = (ServerResponse)iNodeService.findNodeByNodeId(nodeId);
        expectedJson = Obj2Json(serverResponse);
        String uri = "/nodes/show_node_and_alarms.do?nodeId=" + nodeId;
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON))
                .andReturn();
        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();

        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        Assert.assertTrue("correct", expectedJson.equals(content));
        Assert.assertFalse("error", !expectedJson.equals(content));
        System.out.println(content);
    }

    // POST Tests
    @Test
    public void testAddNode() throws Exception {
        //test params
        String nodeName = "nodeForTest1";
        String nodeIdentifier = "nodeIdenForTest1";
        String hubName = "newHubNameForTest";

        String uri = "/nodes/add_node.do";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post(uri)
                .param("nodeName", nodeName)
                .param("nodeIdentifier", nodeIdentifier)
                .param("hubName", hubName))
                .andReturn();

        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        System.out.println(content);
    }


    // PUT Tests
    @Test
    public void testUpdateNodeName() throws Exception {
        // test params
        Integer id = 4;
        String newNodeName = "newNodeForTest";

        String uri = "/nodes/update_node_name.do";
        int status = this.mvc.perform(MockMvcRequestBuilders.post(uri)
                .param("id", String.valueOf(id))
                .param("newNodeName", newNodeName))
                .andReturn().getResponse().getStatus();
        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
    }

    // DELETE Tests
    @Test
    public void testDeleteNode() throws Exception {
        // test params
        String nodeName = "nodeForTest1";

        String uri = "/nodes/delete_node.do";
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.delete(uri)
                .param("nodeName",nodeName)).andReturn();
        Integer status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
        Assert.assertTrue("correct, status is 200", status == 200);
        Assert.assertFalse("error, status should be 200", status != 200);
        System.out.println(content);

    }

    private String Obj2Json(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }

}
