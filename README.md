# DAS Backend API for Groovy Assessment #

This is a coding part for Groovy Assessment Part-2. This application was created using spring-boot and mybatis. It can be run without any additional tools

### Running ###

Application can be run using maven:
```
./mvnw spring-boot:run

```
This starts the application and user can test the api based on api.html document which is placed in ../src/resources/static folder

### Import das.sql ###

Note: please import das.sql into your MySQL server for testing.
das.sql is placed in ../src/resources/static folder.
